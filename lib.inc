section .text
%define SYSCALL_EXIT 60
%define SYSCALL_WRITE 1
%define STDOUT 1
%define STRING_END 0
%define FUNCTION_SUCCESS 1

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi
    .counter
        cmp byte [rax], STRING_END
        je .end
        inc rax
        jmp .counter
    .end
        sub rax, rdi
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    
    mov rsi, rdi
    push rsi 
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    ; sub rsp, 1
    ; mov rsp, dil

    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    pop rdi

    ; add rsp, 1
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    ; push 10
    ; mov rsi, rsp

    ; mov rax, 1
    ; mov rdi, 1
    ; mov rdx, 1
    ; syscall

    ; pop rsi

    ; ret
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    %define .STACK_SIZE 20

    mov rcx, rsp ; put to rcx beginning of stack
    sub rsp, .STACK_SIZE ; reserve 20 bytes for 8 digits 
    mov rax, rdi ; put number to rax
    mov rdi, 10; put 10 to make division
    .loop:
        xor rdx, rdx ; clear rdx
        div rdi ; rax = rax / 10, rdx = rax % 10
        add rdx, '0' ; convert to ASCII
        dec rcx ; move to next byte
        mov [rcx], dl ; put to stack 
        test rax, rax ; check if rax = 0 
        jne .loop ; continue if digits exist
    mov rsi, rcx ; put to rsi beginning of stack


    lea rdx, [rsp + .STACK_SIZE]
    sub rdx, rcx
    
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    add rsp, .STACK_SIZE ; free stack
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print_digit 
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .print_digit:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
        mov dl, [rdi + rcx]
        cmp dl, byte [rsi + rcx]
        jne .not_equal
        inc rcx
        test rdx, rdx
        jne .loop
    .equal:
        mov rax, FUNCTION_SUCCESS
        ret
    .not_equal:
        xor rax, rax
        ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    %define .STACK_SIZE 1
    %define .STRING_LENGTH 1

    sub rsp, .STACK_SIZE
    xor rax, rax
    mov rdx, .STRING_LENGTH
    xor rdi, rdi
    mov rsi, rsp
    syscall
    test rax, rax
    jz .error
    mov al, byte [rsp]
    add rsp, .STACK_SIZE
    ret
    .error:
        add rsp, .STACK_SIZE
        xor rax, rax
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    dec rsi

    %define .BUFF_START r12
    %define .BUFF_SIZE r13
    %define .STRING_LENGTH r14
    push r12
    mov .BUFF_START, rdi
    push r13
    mov .BUFF_SIZE, rsi
    push r14
    xor .STRING_LENGTH, .STRING_LENGTH

    .read_whitespace_loop:
        call read_char
    
        cmp .STRING_LENGTH, .BUFF_SIZE

        jae .buffer_overflow

        cmp al, ` `
        je .read_whitespace_loop
        cmp al, `\t` 
        je .read_whitespace_loop
        cmp al, `\n`  
        je .read_whitespace_loop
        test rax, rax
        jz .end_word

    mov [.BUFF_START + .STRING_LENGTH], al
    inc .STRING_LENGTH

    .read_char_loop:
        call read_char

        cmp .STRING_LENGTH, .BUFF_SIZE
        jae .buffer_overflow

        cmp al, ` `
        je .end_word
        cmp al, `\t` 
        je .end_word
        cmp al, `\n`  
        je .end_word
        cmp al , `\r` 
        je .end_word
        test rax, rax
        jz .end_word
        
        mov [.BUFF_START + .STRING_LENGTH], al
        inc .STRING_LENGTH

        jmp .read_char_loop
        
    .buffer_overflow:
        xor rax, rax
        jmp .end
    .end_word:
        mov byte [.BUFF_START + .STRING_LENGTH], STRING_END
        mov rax, .BUFF_START
        mov rdx, .STRING_LENGTH
    .end:
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    %define .IMUL_CHAR 10
    xor rax, rax
    xor rsi, rsi
    xor rdx, rdx

    .digit:
        ; Загружаем текущий символ
        mov sil, byte [rdi + rdx]

        ; Проверяем, является ли символ цифрой (0-9)
        cmp sil, '0'
        jb .done   ; Если символ не цифра, завершаем разбор
        cmp sil, '9'
        ja .done
        

        ; Умножаем текущее число в rax на 10 и добавляем новую цифру
        imul rax, rax, .IMUL_CHAR
        sub sil, '0'
        add rax, rsi

        ; ; Увеличиваем длину числа
        inc rdx

        ; ; Переходим к следующему символу
        ; inc rdi
        jmp .digit

    .done:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ; mov rax, 0
    %define .IMUL_CHAR -1
    xor rsi, rsi
    ; mov rdx, 0

    mov sil, byte [rdi]
    
    cmp sil, '-'
    je .negative
    jmp .pozitive

    .negative:
        inc rdi
        call parse_uint        
        inc rdx
        imul rax, rax, .IMUL_CHAR
        ret

    .pozitive:
        call parse_uint
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    ; sub rdx, 1
    .loop:
        mov al , [rdi + rcx]
        ; cmp bl, 0
        test rax, rax
        jz .end
        cmp rcx, rdx
        jae .buffer_overflow
        mov [rsi + rcx], al
        inc rcx
        jmp .loop
    .end:
        mov byte [rsi + rcx], STRING_END
        mov rax, rcx
        ret
    .buffer_overflow:
        xor rax, rax
        ret
